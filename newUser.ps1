#Requires -RunAsAdministrator
# newUser.ps1 - A simple script to interactive add a new local user and set the password
#
# Written for my class at ASU: IFT 103
#
# v1.0.2, Kyle Krattiger, 28 Feb. 2021, 13:54 PST

### Variables

$User = "jeff"
$Pass = "a"
#$PassConfirm = "b"
#$LoopFlag = 1

### Main

# Get username
$User = Read-Host -Prompt "Please enter the name of the new user: "

# Loop until good password is given
#while ($LoopFlag -eq 1) {
    $Pass = Read-Host -Prompt "Please enter the password for the user: " -AsSecureString
#    $PassConfirm = Read-Host -Prompt "   Please type password again:" -AsSecureString
#    if ($Pass -eq $PassConfirm) {
#        $LoopFlag = 0
#    }
#    else {
#        Write-Host "The passwords do not match!"
#    }
#}

# Now, add the user and assign the password
if (! (New-LocalUser -Name "$User" -Password $Pass)) {
    Write-Host "User $User could not be created!"
    exit 1
}
else {
    Write-Host "User $User was created successfully!"
    exit 0
}

#EOF