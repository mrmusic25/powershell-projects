#Requires -RunAsAdministrator
#
# newSecurityGroup.ps1 - Simple script to create a new security group and add a user to it
#
# Written for my class at ASU: IFT 103
#
# v1.0.4, Kyle Krattiger, 28 Feb. 2021, 14:36 PST

### Variables

$Username = Read-Host -Prompt "Please enter the username: "
$Groupname = Read-Host -Prompt "Enter the name of the group you wish to add: "
$Folder = "C:\AssignmentB"

### Main

# Make sure user exists
$User = Get-LocalUser -Name "$Username"
if ($null -eq $User) {
    Write-Host "User with name $Username could not be found!"
    exit 1
}

# Create the group
if (! (New-LocalGroup -Name "$Groupname")) {
    Write-Host "Could not create local group $Groupname!"
    exit 1
}
$Group = Get-LocalGroup -Name "$GroupName"

# Add the user to the new group
if (! (Add-LocalGroupMember -Group $Group -Member $User)) {
    Write-Host "User $User could not be added to group $Group!"
    #exit 1
}

# Create a new folder
if (! (Test-Path -Path "$Folder")) {
    Write-Host "Path $Folder does not exist, creating!"
    New-Item -ItemType Directory -Path "$Folder"
}

# Add group to the new folder and exit based on result
$ACL = Get-Acl -Path "$Folder"
$Rule = New-Object System.Security.AccessControl.FileSystemAccessRule($Group,"FullControl","allow")
if (! ($ACL.AddAccessRule($Rule))) {
    Write-Host "Could not add new access rule to ACL for $Folder!"
    #exit 1
}

# Finally, write the new ACL to the folder
if (! ($ACL | Set-Acl -Path "$Folder")) {
    Write-Host "Could not set the new ACL to folder $Folder!"
    exit 1
}
else {
    Write-Host "Script completed successfully!"
    exit 0
}

#EOF